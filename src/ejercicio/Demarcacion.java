package ejercicio;

public enum Demarcacion {
	PORTERO, DEFENSA, CENTROCAMPISTA;

	public static Demarcacion getPosicion(String posicion) {
		Demarcacion salida = null;
		if ("P".equals(posicion)) {
			salida = Demarcacion.PORTERO;
		}

		else if ("D".equals(posicion)) {
			salida = Demarcacion.DEFENSA;
		} else if ("C".equals(posicion)) {
			salida = Demarcacion.CENTROCAMPISTA;
		}
		return salida;
	}

}
